#!/bin/sh

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import fcntl
import jinja2
import socket
import struct
import sys
import yaml


def get_ip_address(ifname):
    # return netifaces.ifaddresses(ifname)[netifaces.AF_INET][0]['addr']
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        addr = socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', bytes(ifname[:15], 'utf-8'))
        )[20:24])
    return addr


def generate_dhcp_conf():
    with open('/etc/dhcpd.j2', 'r') as fd:
        template = fd.read()
    with open('/config/dhcp.yaml', 'r') as fd:
        params = yaml.safe_load(fd)
    ironic = params.setdefault('ironic', {})
    if 'ip' not in ironic:
        ironic['ip'] = get_ip_address(sys.argv[1])
    if 'port' not in ironic:
        ironic['port'] = '6180'
    t = jinja2.Template(template)
    print(t.render(params))
    with open('/etc/dhcpd.conf', 'w') as fd:
        fd.write(t.render(params))


if __name__ == "__main__":
    generate_dhcp_conf()
